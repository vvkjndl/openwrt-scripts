#!/bin/sh

opkg update

mkdir -p /root/logs/
opkg list-upgradable > /root/logs/opkg-list-upgradable.log

opkg install \
	acme \
	arptables \
	block-mount \
	bsdtar \
	collectd-mod-cpu \
	collectd-mod-df \
	collectd-mod-disk \
	collectd-mod-dns \
	collectd-mod-entropy \
	collectd-mod-interface \
	collectd-mod-iptables \
	collectd-mod-iwinfo \
	collectd-mod-load \
	collectd-mod-memory \
	collectd-mod-network \
	collectd-mod-openvpn \
	collectd-mod-ping \
	collectd-mod-processes \
	collectd-mod-rrdtool \
	collectd-mod-tcpconns \
	collectd-mod-uptime \
	collectd-mod-wireless \
	coreutils-stat \
	curl \
	ddns-scripts \
	diffutils \
	drill \
	e2fsprogs \
	htop \
	iftop \
	kmod-fs-ext4 \
	kmod-usb-storage \
	luci-app-acme \
	luci-app-advanced-reboot \
	luci-app-ddns \
	luci-app-openvpn \
	luci-app-privoxy \
	luci-app-statistics \
	luci-app-vnstat \
	luci-app-wireguard \
	mtr \
	nano \
	nftables \
	openvpn-openssl \
	privoxy \
	procps-ng-watch \
	rsync \
	strace \
	stubby \
	stunnel \
	sysstat \
	tcpdump \
	tcpreplay \
	tor \
	uboot-envtools \
	udpxy \
	vnstat \
	vnstati \
	wget \
	wireguard \
	wireguard-tools