#!/bin/sh

backdir=/root/backup/
backfile=backup-$(uci get system.@system[0].hostname)
backinclude=/root/scripts/backup-files-include
backexclude=/root/scripts/backup-files-exclude

# tar backup with gzip and checksum generation
sysupgrade --list-backup > $backinclude
tar c -v -T $backinclude -X $backexclude -f - | gzip > $backdir/$backfile.tar.gz
echo "$(sha256sum $backdir/$backfile.tar.gz | cut -d ' ' -f1)  $backfile.tar.gz" > $backdir/$backfile.tar.gz.sha256sum